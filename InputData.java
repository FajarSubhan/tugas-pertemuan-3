/**
 * @Author  Fajar Subhan
 * @Npm     202043500578
 *
 */

/* Import Java Scanner Class */
import java.util.Scanner;

public class InputData
{
    public static void main(String[] data)
    {
        Scanner input       = new Scanner(System.in);

        System.out.print("Jumlah Data : ");

        int count = input.nextInt();

        System.out.println("----------------------------------------------------------");

        String  subject[][]     = new String[count][4];
        Float   number1[][]     = new Float[count][4];
        Float   number2[][]     = new Float[count][4];
        Float   avg[][]         = new Float[count][4];


        for(int i = 0; i < count; i++)
        {
            System.out.print("Mata Kuliah       : ");
            subject[i][0] = input.next();

            System.out.print("Nilai Teori       : ");
            number1[i][1] = input.nextFloat();

            System.out.print("Nilai Praktikum   : ");
            number2[i][2] = input.nextFloat();

            avg[i][3]    = (number1[i][1] + number2[i][2]) / 2;

            System.out.println();
        }

        System.out.println("--------------------------------------------------");
        System.out.println("Mata Kuliah\tTeori\tPratikum\tRata-Rata");
        System.out.println("--------------------------------------------------");

        for(int x = 0;x < count;x++)
        {
            System.out.print(subject[x][0] + "\t\t\t" + String.format("%.0f",number1[x][1]) + "\t\t\t" + String.format("%.0f",number2[x][2])  + "\t\t\t"  + avg[x][3]);

            System.out.println();
        }


    }
}
